unix{
    macx {
        installer.target = $${TARGET}.app
        installer.target.path = installer
        installer.commands = @echo Building $$installer.target $$escape_expand(\n\t)\
                             mkdir $${installer.target.path} $$escape_expand(\n\t)\
                             cp -r $${TARGET}.app $${installer.target.path}/ $$escape_expand(\n\t)\
                             $(QTDIR)/bin/macdeployqt $${installer.target.path}/$${TARGET}.app -dmg -qmldir=$${PWD}
        QMAKE_CLEAN += $${installer.target.path}
        QMAKE_DEL_FILE = rm -rf

    }else{
        installer.target = $${TARGET}.rpm
        installer.commands = @echo Building RPM $$installer.target
    }
}

QMAKE_EXTRA_TARGETS += installer
QMAKE_CLEAN += $${installer.target}
