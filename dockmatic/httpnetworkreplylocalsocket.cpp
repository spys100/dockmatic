#include "httpnetworkreplylocalsocket.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(localSocket, "localSocket")

QList<QLocalSocket*> HttpNetworkReplyLocalSocket::m_socks;
static const QString nl=QStringLiteral("\015\012");

HttpNetworkReplyLocalSocket::HttpNetworkReplyLocalSocket(const QNetworkAccessManager *manager,  QNetworkAccessManager::Operation op,
                                                         const QNetworkRequest &request,
                                                         QIODevice *outgoingData)
    : m_readMode(HEADER)
{
    open(QIODevice::ReadOnly); // just mark as open
    m_sock = new QLocalSocket(this);
    m_sock->setServerName(request.url().userInfo());
    m_socks.append( m_sock );

    qCDebug(localSocket) << "open socket: " << m_sock;
    connect(m_sock, &QLocalSocket::readyRead, this, &HttpNetworkReplyLocalSocket::onDataAvailable);
    connect(m_sock, &QLocalSocket::aboutToClose, this, &QNetworkReply::aboutToClose);
    connect(m_sock, &QLocalSocket::readChannelFinished, this, &QNetworkReply::readChannelFinished);
    connect(m_sock, &QLocalSocket::channelReadyRead, this, &QNetworkReply::channelReadyRead);
    connect(m_sock, &QLocalSocket::channelBytesWritten, this, &QNetworkReply::channelBytesWritten);
    connect(m_sock, &QLocalSocket::bytesWritten, this, &QNetworkReply::bytesWritten);
    //connect(m_sock, &QLocalSocket::disconnected, this, &HttpNetworkReplyLocalSocket::close);
    connect(m_sock, static_cast<void(QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error),
          [=](QLocalSocket::LocalSocketError socketError){
        qCWarning(localSocket) << m_sock << m_sock->errorString();
        emit error();
    });
    this->setRequest(request);
    if (!m_sock->isOpen()){
        m_sock->connectToServer();
        if (m_sock->waitForConnected(1000))
             qCDebug(localSocket) << m_sock << "Connected!";
        else{
            qCDebug(localSocket) << m_sock << "Connection timed out.";
        }
    }
    QByteArray ba;
    switch (op) {
    case QNetworkAccessManager::HeadOperation:
        ba = "HEAD ";
        break;
    case QNetworkAccessManager::GetOperation:
        ba = "GET ";
        break;
    case QNetworkAccessManager::PutOperation:
        ba = "PUT ";
        break;
    case QNetworkAccessManager::PostOperation:
        ba = "POST ";
        break;
    case QNetworkAccessManager::DeleteOperation:
        ba = "DELETE ";
        break;
    case QNetworkAccessManager::CustomOperation:
    case QNetworkAccessManager::UnknownOperation:
        break;

    }

    ba += request.url().path(QUrl::FullyEncoded).toUtf8();
    if (request.url().hasQuery()){
        ba += "?" + request.url().query(QUrl::FullyEncoded).toUtf8();
    }
    ba += " HTTP/1.1" + nl;
    ba += "Host: localhost" + nl;
    ba += "User-Agent: dockmatic" + nl;
    ba += nl;
    int send_bytes = m_sock->write(ba);
    qCDebug(localSocket) << ba << send_bytes << " bytes sent.";
}

HttpNetworkReplyLocalSocket::~HttpNetworkReplyLocalSocket()
{
    qCDebug(localSocket) << "close socket: " << m_sock;
    m_socks.removeOne(m_sock);
}

// a very basic http parser

void HttpNetworkReplyLocalSocket::parseHeader()
{
    qint64 bytesRead = 0;
    qint64 contentLength = -1;
    const int MAX_HEAD = 2048;

    do{
           char _header[MAX_HEAD];
           bytesRead = m_sock->readLine(_header, MAX_HEAD);
           QString header(_header);
           header = header.left(header.size()-2); //strip crlf
           QString hname, hvalue;
           {
               QStringList hr = header.split(':');
               hname = hr.value(0);
               hvalue = hr.value(1).trimmed();
           }
           qCDebug(localSocket) << m_sock << "= RespHeader:" << header;

           //status code
           if ( hname.startsWith("HTTP")) {
               int status = hname.split(' ').value(1).toInt();
               setAttribute(QNetworkRequest::HttpStatusCodeAttribute, status);
           } else if ( header.isEmpty() ){
               break; //end of header marker
           } else if (hname.startsWith("Content-Length",Qt::CaseInsensitive) ){
               contentLength = hvalue.toInt();
               setHeader(QNetworkRequest::ContentLengthHeader, contentLength);
           } else {
               setRawHeader(hname.toUtf8(),hvalue.toUtf8());
           }
       } while ( bytesRead > 0);
}

qint64 HttpNetworkReplyLocalSocket::readData(char *data, qint64 maxlen)
{
    maxlen = qMin(qint64(m_content.size()), maxlen);
    if (maxlen <= 0) return 0;
    memcpy(data, m_content.constData(), maxlen);
    m_content = m_content.mid(maxlen);
    return maxlen;
}

qint64 HttpNetworkReplyLocalSocket::readChunked()
{
    qint64 bytesRead = 0;
    const int MAX_CHUNCK_SIZE = 2048;
    char _chunk[MAX_CHUNCK_SIZE];
    bytesRead = m_sock->readLine(_chunk, MAX_CHUNCK_SIZE);
    if (bytesRead > 0){
        QString chunksize(_chunk);
        chunksize = chunksize.left(chunksize.size()-2); //strip crlf
        int cs = chunksize.toInt();
        cs = qMin(cs, MAX_CHUNCK_SIZE);
        if (cs == 0) {
            setFinished(true);
            emit finished();
        }else{
            bytesRead = m_sock->readLine(_chunk, cs);
            m_content.append(_chunk,bytesRead);
            //qCDebug(localSocket) << m_sock << " readChunk[" << cs << "]" << QString(_chunk);
        }
    }
    return bytesRead;
}

qint64 HttpNetworkReplyLocalSocket::readDataInternal()
{
    qint64 bytesRead = 0;

    // chunked response
    if (  rawHeader("Transfer-Encoding") == "chunked" ){
        bytesRead = readChunked();
    }else{
        qint64 bytesToRead = header(QNetworkRequest::ContentLengthHeader).toLongLong() - m_content.size();
        QByteArray ba = m_sock->read(bytesToRead);
        bytesRead = ba.size();
        m_content.append(ba);
    }
    return bytesRead;
}


void HttpNetworkReplyLocalSocket::abort(){
    m_sock->abort();
}

void HttpNetworkReplyLocalSocket::onDataAvailable()
{
    if ( m_readMode == HEADER ){
        parseHeader();
        m_readMode = CONTENT;
    }
    readDataInternal();
    if ( rawHeader("Transfer-Encoding") != "chunked" ){
        if ( header(QNetworkRequest::ContentLengthHeader).toLongLong() == m_content.size() ){
            setFinished(true);
            emit finished();
        }
    }
    emit readyRead();

}
