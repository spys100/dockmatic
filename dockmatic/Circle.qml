/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

import QtQuick 2.0

Rectangle {
     id: circle
     width: 10
     height: width
     color: "#fa4b4b"
     border.width: 4
     radius: width*0.5

     states: [
         State {
             name: "G"

             PropertyChanges {
                 target: circle
                 color: "#12ec37"
             }
         },
         State {
             name: "R"

             PropertyChanges {
                 target: circle
                 color: "#fa4b4b"
             }
         },
         State {
             name: "Y"

             PropertyChanges {
                 target: circle
                 color: "#faef4a"
             }
         },
         State {
             name: "B"

             PropertyChanges {
                 target: circle
                 color: "#535249"
             }
         }
     ]
     transitions: [
         Transition {
             ColorAnimation { target: circle; duration: 100}
         }
     ]
     border.color: "#00000000"
}
