/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#ifndef APPINIT_H
#define APPINIT_H

#include <QApplication>

class DockerHostModel;
class QQmlEngine;
class QJSEngine;
class DockerAPI;
class EventDispatcher;

class AppInit : public QApplication
{
    Q_OBJECT
private:
    DockerHostModel *m_hostList;
    static DockerAPI *m_dockerAPI;
    EventDispatcher *m_dispatcher;
public:
    explicit AppInit(int & argc, char ** argv);
    QString appDataDir() const;
    DockerHostModel *hostList() const;

    // Second, define the singleton type provider function (callback).
    //static QObject *dockerAPI(QQmlEngine *engine, QJSEngine *scriptEngine);
    //static DockerAPI *dockerAPI();
signals:

public slots:
    void saveAll();
    void loadHosts();
};

#undef qApp
#define qApp static_cast<AppInit*>(QCoreApplication::instance())
#undef _qApp

#endif // APPINIT_H
