/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#ifndef DOCKERHOST_H
#define DOCKERHOST_H

#include <QString>
#include <QUrl>
#include <QPointer>

#include "dockerapi.h"
#include "containermodel.h"

class QNetworkReply;
class CustomNetworkAccessManager;
class DockerHost;
class QWebSocket;

typedef QSharedPointer<DockerHost> DockerHostPtr;

class DockerHost : public DockerAPI
{
    Q_OBJECT
    Q_PROPERTY(ContainerModel containerModel READ containerModel)
private:
    QString m_alias;   

    QNetworkReply *m_eventReply;
    static CustomNetworkAccessManager *m_networkmanager;

    ContainerModel *m_containerModel;
private:
    void addContainer(const QJsonObject& container);
    virtual void sendRequest( DC::DockerCommandId cid, const QString &path, const t_paramList &params = t_paramList()) Q_DECL_OVERRIDE;
    virtual void attachConsole(QWebSocket *ws, const QString &title) Q_DECL_OVERRIDE;
private slots:
    void onDockerEvent();
    void replyFinished(QNetworkReply *reply);
    void authenticate(QNetworkReply* repl,QAuthenticator* auth);
    void onSslErrors(QNetworkReply* reply,QList<QSslError> errors);

public:
    friend QDataStream & operator<< (QDataStream& stream, const DockerHost& dh);
    friend QDataStream & operator>> (QDataStream& stream, DockerHost& dh);
    friend QDataStream & operator<< (QDataStream& stream, const DockerHostPtr& dh);
    friend QDataStream & operator>> (QDataStream& stream, DockerHostPtr& dh);

    explicit DockerHost();

    QString hostName() const
    {   if ( isLocalConnection() ){
            return m_baseUrl.userInfo();
        }else{
            return m_baseUrl.host();
        }
    }
    void setHostName(const QString& val){
        if (val.startsWith('/')){
            // use the userinfo part for local socket connections
            QString v = QUrl::toPercentEncoding(val);
            m_baseUrl.setUserInfo(v, QUrl::TolerantMode);
        }else{
            m_baseUrl.setHost(val);
        }
    }
    int port() const { return m_baseUrl.port(); }
    void setPort(const int val){ m_baseUrl.setPort(val); }
    QByteArray key() const;
    void setKey(const QByteArray &val);
    QByteArray cert() const;
    void setCert(const QByteArray& val);
    QByteArray ca() const;
    void setCa(const QByteArray &val);
    QString alias() const { return m_alias; }
    void setAlias(const QString& val){ m_alias = val; }

    void connectTo();
    ContainerModel* containerModel() const
    {
        return m_containerModel;
    }
    /**
     * @brief isLocalConnection
     * @return true if the connection is a local unix socket connection
     */
    bool isLocalConnection() const
    {
        return m_baseUrl.userInfo().startsWith('/');
    }
};

#endif // DOCKERHOST_H
