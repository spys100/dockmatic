/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include "containermodel.h"



ContainerModel::ContainerModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

ContainerModel::ContainerModel(const ContainerModel &other)
{
    *this = other;
}

ContainerModel& ContainerModel::operator=(const ContainerModel &other)
{
    static_cast<QObject*>(this)->setParent(other.parent());
    m_containerList = other.m_containerList;
    return *this;
}

QHash<int, QByteArray> ContainerModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IDRole] = "id";
    roles[NamesRole] = "names";
    roles[ImageRole] = "image";
    roles[CommandRole] = "command";
    roles[CreatedRole] = "created";
    roles[PortsRole] = "ports";
    roles[LabelsRole] = "labels";
    roles[StatusRole] = "status";
    return roles;
}

int ContainerModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_containerList.count();
}

QVariant ContainerModel::data(const QModelIndex &index, int role) const
{
    QVariant val;
    if (index.isValid()){
        const Container &container = m_containerList.at(index.row());

        switch (role) {
        case IDRole:
            val.setValue(container.id());
            break;
        case NamesRole:
            val.setValue(container.names());
            break;
        case ImageRole:
            val.setValue(container.image());
            break;
        case CommandRole:
            val.setValue(container.command());
            break;
        case CreatedRole:
            val.setValue(container.created());
            break;
        case PortsRole:
            val.setValue(container.ports());
            break;
        case LabelsRole:
            val.setValue(container.labels());
            break;
        case StatusRole:
            val.setValue(container.status());
            break;
        default:
            break;
        }
    }
    return val;

}

bool ContainerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    bool rv = true;
    if (index.isValid()){
        Container &container = m_containerList[index.row()];
        switch (role) {
        case IDRole:
            container.setId(value.toString());
            break;
        case NamesRole:
            container.setNames(value.toStringList());
            break;
        case ImageRole:
            container.setImage(value.toString());
            break;
        case CommandRole:
            container.setCommand(value.toString());
            break;
        case CreatedRole:
            container.setCreated(value.toDateTime());
            break;
        case PortsRole:
            //container.setPorst(value.toList());
            break;
        case LabelsRole:
            container.setLabels(value.toStringList());
            break;
        case StatusRole:
            container.setStatus(value.toString());
            break;
        default:
            rv = false;
            break;
        }
        emit dataChanged(index, index);
    }else{
        rv = false;
    }

    return rv;
}

bool ContainerModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent,row, row + count - 1);
    for (; count > 0; count--)
        m_containerList.insert(row, Container());
    endInsertRows();
    return true;

}

bool ContainerModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent,row, row + count - 1);
    for (; count > 0; count--)
        m_containerList.removeAt(row + count - 1);
    endRemoveRows();
    return true;
}

QModelIndex ContainerModel::indexById(const QString &id)
{
    QModelIndexList idxs =  match(index(0) ,IDRole, id);
    QModelIndex idx = idxs.isEmpty() ? createIndex(-1,-1) : idxs.first() ;
    return idx;
}
