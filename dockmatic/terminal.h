/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#ifndef TERMINAL
#define TERMINAL


#include <QObject>
#include <QIODevice>
#include <QQueue>
#include <QThread>
#include <QWaitCondition>
#include <QReadWriteLock>

class QTermWidget;
class QWebSocket;

class TerminalProxy :public QObject {
    Q_OBJECT
public:
    TerminalProxy(QWebSocket *ws, const QString &title);
    virtual ~TerminalProxy();
private:
    QTermWidget* m_console;

    class OutQueue: public QThread {
    public:
        OutQueue(QObject *parent, int fd);
        virtual ~OutQueue() Q_DECL_OVERRIDE;
        virtual void run() Q_DECL_OVERRIDE;
        void write(const QByteArray& ba);
    private:
        QQueue<QByteArray> m_outQueue;
        int m_fd;
        QWaitCondition m_avail;
        QReadWriteLock m_rwLock;
    } *m_outQueue;
};

#endif // TERMINAL

