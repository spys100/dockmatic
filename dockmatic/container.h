/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#ifndef CONTAINER_H
#define CONTAINER_H

#include <QString>
#include <QStringList>
#include <QDateTime>

class Container
{
    QString m_ID;
    QStringList m_names;
    QString m_image;
    QString m_command;
    QDateTime m_created;
    QList<int> m_ports;
    QStringList m_labels;
    QString m_status;

public:
    explicit Container();
    Container(const QString& id, const QStringList& names, const QString& image,
              const QString& command, const QDateTime& created, const QList<int>& ports,
              const QStringList& labels, const QString& status);
    QString id() const;
    void setId(const QString& id);
    QStringList names() const;
    void setNames(const QStringList& names);
    QString image() const;
    void setImage(const QString& image);
    QString command() const;
    void setCommand(const QString& command);
    QDateTime created() const;
    void setCreated(const QDateTime& created);
    QList<int> ports() const;
    void setPorst(const QList<int>& ports);
    QStringList labels() const;
    void setLabels(const QStringList& labels);
    QString status() const;
    void setStatus(const QString& status);

};

#endif // CONTAINER_H
