/*  Copyright (C) 2008 e_k (e_k@users.sourceforge.net)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "qtermwidget.h"
#include "terminal.h"
#include <unistd.h>

#include <QApplication>
#include <QtDebug>
#include <QBuffer>
#include <QWebSocket>

TerminalProxy::TerminalProxy(QWebSocket *ws, const QString& title)
    : QObject(ws)
{
    qDebug() << Q_FUNC_INFO << this;
    QIcon::setThemeName("oxygen");


    m_console = new QTermWidget(0);

    QFont font = QApplication::font();
#ifdef Q_OS_MAC
    font.setFamily("Monaco");
#elif defined(Q_OS_WIN)
    font.setFamily("fixed");
#else
    font.setFamily("Monospace");
#endif
    font.setPointSize(12);

    m_console->setTerminalFont(font);
    m_console->startTerminalTeletype();
    m_console->setSize(80, 25);
/*
    QMenuBar *menuBar = new QMenuBar(mainWindow);
    QMenu *actionsMenu = new QMenu("Actions", menuBar);
    menuBar->addMenu(actionsMenu);
    actionsMenu->addAction("Find..", console, SLOT(toggleShowSearchBar()), QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_F));
    actionsMenu->addAction("About Qt", &app, SLOT(aboutQt()));
    mainWindow->setMenuBar(menuBar);
*/

   // console->setColorScheme(COLOR_SCHEME_BLACK_ON_LIGHT_YELLOW);
    m_console->setScrollBarPosition(QTermWidget::ScrollBarRight);

    // info output
    qDebug() << "* INFO *************************";
    qDebug() << " availableKeyBindings:" << m_console->availableKeyBindings();
    qDebug() << " keyBindings:" << m_console->keyBindings();
    qDebug() << " availableColorSchemes:" << m_console->availableColorSchemes();
    qDebug() << "* INFO END *********************";

    m_console->setColorScheme("DarkPastels");
    m_console->setWindowTitle(title);
    m_console->setAttribute(Qt::WA_DeleteOnClose);

    connect(m_console, &QTermWidget::destroyed, this, [=](){
        m_console = NULL;
        this->deleteLater();
    });

    connect(m_console, &QTermWidget::sendData, this, [=] (const char* c,int sz){
        QByteArray ba(c, sz);
        ws->sendBinaryMessage(ba);
    });
    connect(ws, &QWebSocket::textMessageReceived, this, [=] (QString msg){
        QByteArray ta = msg.toUtf8();
        //do the write in a seperate thread because
        //it can potentially block the main thread
        //and this leads to a deadlock. The reading is done in the same thread...
        //write(fd, ta.constData(), ta.size());
        m_outQueue->write(ta);
    });

    m_outQueue = new OutQueue(this ,m_console->getPtySlaveFd());

    m_console->show();
}

TerminalProxy::~TerminalProxy()
{
    delete m_console;
}


TerminalProxy::OutQueue::OutQueue(QObject *parent, int fd)
    :QThread(parent)
    ,m_fd(fd)
{

}

TerminalProxy::OutQueue::~OutQueue()
{
    //wait 5 seconds for queue thread to end
    if (isRunning()) {
        requestInterruption();
        write(0);//wakes the thread
        wait(5000);
    }
}

void TerminalProxy::OutQueue::run()
{
    forever {
        m_rwLock.lockForRead();
        if (m_outQueue.isEmpty()) {
            m_avail.wait(&m_rwLock);
        }
        QByteArray ta = m_outQueue.dequeue();
        m_rwLock.unlock();
        if ( QThread::currentThread()->isInterruptionRequested() ) {
            return;
        }
        ::write(m_fd, ta.constData(), ta.size());

    }
}

void TerminalProxy::OutQueue::write(const QByteArray &ba)
{
    m_rwLock.lockForWrite();
    m_outQueue.enqueue(ba);
    m_avail.wakeAll();
    m_rwLock.unlock();
    start();
}
