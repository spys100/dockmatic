#ifndef SETTINGS_H
#define SETTINGS_H

/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include <QAbstractListModel>
#include <QList>
#include <QSharedPointer>

//#include "dockerhost.h"
class DockerHost;

typedef QSharedPointer<DockerHost> DockerHostPtr;

class DockerHostModel : public QAbstractListModel
{
    Q_OBJECT
public:
    friend QDataStream & operator<< (QDataStream& stream, const DockerHostModel& model);
    friend QDataStream & operator>> (QDataStream& stream, DockerHostModel& model);
    enum SettingsRoles {
        HostNameRole = Qt::UserRole + 1,
        PortRole,
        KeyRole,
        CertRole,
        CaRole,
        AliasRole,
        ContainerModelRole,
        DockerHostRole
    };
    explicit DockerHostModel(QObject *parent = 0);
    QHash<int, QByteArray> roleNames() const;

    virtual int rowCount(const QModelIndex &parent=QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const {Q_UNUSED(index);return Qt::ItemIsEditable;}

    virtual bool insertRows(int row, int count, const QModelIndex &parent);
    virtual bool removeRows(int row, int count, const QModelIndex &parent);

    QList<DockerHostPtr> model() const;
private:
    QList<DockerHostPtr> m_hostList;
signals:

public slots:
    void createItem();
    void removeItem(int i);
};



#endif // SETTINGS_H
