/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import com.ximidi.FileReader 1.0
import Material 0.1

RowLayout{
    id: dockerHostItem
    spacing: Units.dp(6)

    width:parent.width

    //color: "#ffffff"
    property string host
    property int port
    property string certFile
    property string keyFile
    property string caFile

    //signal receivedJsonReply()
    signal connectTo()

    onConnectTo: {
        dockerHost.connectTo(host, port, certFile, keyFile, caFile)
    }

    Component.onCompleted: {

    }

    Circle {
        id: status
        border.width: 4
    }

    TextField {
        id: textFieldHost
        Layout.fillWidth: true
        //Layout.preferredWidth: Units.dp(50)
        Layout.minimumWidth: textFieldHost.implicitWidth
        placeholderText: "192.168.3.10"
        text: host
        onTextChanged: host = text
    }

    TextField {
        id: textFieldPort
        Layout.preferredWidth: Units.dp(40)
        placeholderText: "2376"
        text: port == 0 ? "" : port
        onTextChanged: port = text
    }

    Button {
        id: buttonKey

        //x: 106
        //y: 8
        elevation: 1
        text: qsTr("Key")
        //anchors.top: parent.top
        onClicked: {
            dlg.title=qsTr("Load Certificate")
            dlg.type=1
            dlg.open()
        }
    }

    Button {
        id: buttonCert
        //x: 183
        elevation: 1
        text: qsTr("Certificate")
        //anchors.top: parent.top
        //anchors.topMargin: 0
        onClicked: {
            dlg.title=qsTr("Load Certificate")
            dlg.type=0
            dlg.open()
        }
    }

    Button {
        id: buttonCa
        //x: 183
        elevation: 1
        text: qsTr("CA")
        //anchors.top: parent.top
        //anchors.topMargin: 0
        onClicked: {
            dlg.title=qsTr("Load CA")
            dlg.type=2
            dlg.open()
        }
    }

    FileReader {
        id: fileReader
    }

    FileDialog {
        id: dlg
        property int type: 0
        onAccepted: {
            var f = dlg.fileUrl.toString().substring(7);
            if (type == 0){
                certFile = fileReader.read(f);
            }
            if (type == 1)
                keyFile = fileReader.read(f);
            if (type == 2)
                caFile = fileReader.read(f);

        }
        onRejected: {

        }
    }
}
