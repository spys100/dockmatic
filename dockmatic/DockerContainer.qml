/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

import QtQuick 2.3
import QtQuick.Layouts 1.1
import Material 0.1

RowLayout {
    id: dockerContainer
    property alias name: name.text
    property var labels
    property string image
    property string status: "unknown"

    signal stop
    signal start
    signal pause
    signal restart
    signal unpause
    signal openTerminal

    onStatusChanged: {
        if ( status.match("Exited|^stop$") ){
            state = "stopped";
        }else if (status.match("\(Paused\)|^pause$")){
            state = "paused"
        }else if (status.match("Up|^start$|^unpause$|^restart$")){
            state = "running"
        }else{
            state = "unknown"
        }
    }

    Circle {
        id: statusIndicator
    }

    Label {
        id: name
        Layout.minimumWidth: 100
    }

    Repeater{
        model: labels
        Label {
            text: model
            Layout.minimumWidth: implicitWidth
        }
    }

    IconButton {
        id: startStopButton
        iconName: "av/play_circle_fill"
        onClicked: {
            if (dockerContainer.state == "running"){
                stop();
            }else if(dockerContainer.state == "paused"){
                unpause();
            }else{
                start();
            }
        }
    }

    IconButton {
        id: pauseButton
        iconName: "av/pause_circle_fill"
        onClicked: pause()
    }

    IconButton {
        id: restartButton
        iconName: "av/replay"
        onClicked: restart()
    }

    IconButton {
        id: terminalButton
        iconName: "hardware/computer"
        onClicked: openTerminal()
    }
    states: [
        State {
            name: "unknown"
            PropertyChanges {
                target: statusIndicator
                state: "B"
            }
        },
        State {
            name: "running"
            StateChangeScript {
                script: {
                    console.log("running");
                }
            }

            PropertyChanges {
                target: statusIndicator
                state: "G"
            }
            PropertyChanges {
                target: startStopButton
                iconName: "av/stop"
            }
            PropertyChanges {
                target: pauseButton
                enabled: true
            }
        },
        State {
            name: "stopped"

            PropertyChanges {
                target: statusIndicator
                state: "R"
            }
            PropertyChanges {
                target: startStopButton
                iconName: "av/play_circle_fill"
            }
            PropertyChanges {
                target: pauseButton
                enabled: false
            }
        },
        State {
            name: "paused"

            PropertyChanges {
                target: statusIndicator
                state: "Y"
            }
            PropertyChanges {
                target: pauseButton
                enabled: false
            }
        }
    ]

}

