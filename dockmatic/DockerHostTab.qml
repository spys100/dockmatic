/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

import QtQuick 2.0
import Material 0.1
import Material.ListItems 0.1 as ListItem
import com.ximidi.DockerHostModel 1.0
import QtQuick.Layouts 1.1

Item {
    id: hostTab
    Flickable {
        id: flickable
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        clip: true
        contentHeight: Math.max(appwin.implicitHeight + 40, height)
        ColumnLayout {
            anchors {
                left: parent.left
                right: parent.right
                margins: Units.dp(6)
            }
            spacing: Units.dp(6)
            Repeater {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                /// the model is defined externally in C++ code
                model: dockerHostList
                ListItem.Standard {
                        RowLayout {
                            width: parent.width
                            spacing: Units.dp(12)
                            DockerHost {

                                Component.onCompleted: {
                                    host = model.hostName
                                    port = model.port
                                }
                                onHostChanged: model.hostName = host
                                onPortChanged: model.port = port
                                onCertFileChanged: model.cert = certFile
                                onCaFileChanged: model.ca = caFile
                                onKeyFileChanged: model.key = keyFile

                               /* anchors {
                                    leftMargin: 17
                                    left: parent.left
                                    right: parent.right
                                }*/


                            }
                            IconButton {
                                iconName: "awesome/minus_square_o"
                                onClicked: dockerHostList.removeItem(model.index)
                            }
                        }
                        ThinDivider {
                            anchors {
                                left: parent.left
                                right: parent.right
                                top: parent.top
                            }
                            //visible: header.expanded
                        }
                    }
            }
        }
    }
    Scrollbar {
        flickableItem: flickable
    }
}

