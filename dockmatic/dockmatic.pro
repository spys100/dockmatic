TEMPLATE = app
#QMAKE_MAC_SDK = macosx10.11
CONFIG += c++11

macx-clang:QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-inconsistent-missing-override

QT += qml quick network widgets websockets svg

SOURCES += main.cpp \
    dockerapi.cpp \
    terminal.cpp \
    dockerhostmodel.cpp \
    dockerhost.cpp \
    appinit.cpp \
    container.cpp \
    containermodel.cpp \
    filereader.cpp \
    customnetworkaccessmanager.cpp \
    httpnetworkreplylocalsocket.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

# Rules for creating an installable package
include(package.pri)

HEADERS += \
    dockerapi.h \
    terminal.h \
    dockerhostmodel.h \
    dockerhost.h \
    appinit.h \
    container.h \
    containermodel.h \
    filereader.h \
    customnetworkaccessmanager.h \
    httpnetworkreplylocalsocket.h



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../qtermwidget/release/ -lqtermwidget
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../qtermwidget/debug/ -lqtermwidget
else:unix: LIBS += -L$$OUT_PWD/../qtermwidget/ -lqtermwidget

INCLUDEPATH += $$PWD/../qtermwidget/lib
DEPENDPATH += $$PWD/../qtermwidget/lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtermwidget/release/libqtermwidget.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtermwidget/debug/libqtermwidget.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtermwidget/release/qtermwidget.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtermwidget/debug/qtermwidget.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../qtermwidget/libqtermwidget.a
