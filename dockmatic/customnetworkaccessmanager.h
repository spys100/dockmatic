#ifndef CUSTOMNETWORKACCESSMANAGER_H
#define CUSTOMNETWORKACCESSMANAGER_H
#include <QNetworkAccessManager>

class QNetworkReply;

class CustomNetworkAccessManager : public QNetworkAccessManager
{
public:
    CustomNetworkAccessManager();
protected:
    virtual QNetworkReply *createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData);
};

#endif // CUSTOMNETWORKACCESSMANAGER_H
