#include "appinit.h"
#include "dockerhostmodel.h"
#include "dockerhost.h"
/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include <QDataStream>
#include <QDir>
#include <QTimer>
#include "dockerapi.h"

AppInit::AppInit(int &argc, char **argv) : QApplication(argc, argv)
  ,m_hostList(new DockerHostModel(this))
{
    connect(this, SIGNAL(aboutToQuit()), SLOT(saveAll()));
    loadHosts();
    //connect to all instances
    //and collect info about running containers
    foreach(DockerHostPtr h, m_hostList->model()){
        h->connectTo();
        h->listContainers();
        //listen for events
        h->events();
        //connect(h.data(), SIGNAL(receivedJsonReply(int,QString)), h, SLOT(onJsonEvent(int ,QString)));
    }
}

QString AppInit::appDataDir() const
{
    QString data = QDir::homePath() +  QDir::separator() + "." + applicationName();
    QDir::home().mkdir(data);
    return data;
}

DockerHostModel *AppInit::hostList() const
{
    return m_hostList;

}

DockerAPI* AppInit::m_dockerAPI = NULL;

/*
QObject *AppInit::dockerAPI(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    if (! m_dockerAPI){
        m_dockerAPI = new DockerAPI(qApp);
    }
    //SingletonTypeExample *example = new SingletonTypeExample();
    return m_dockerAPI;
}
*/

/*
DockerAPI *AppInit::dockerAPI()
{
    return static_cast<DockerAPI*>(dockerAPI(NULL, NULL));
}
*/

void AppInit::saveAll()
{
    QFile file(appDataDir() + QDir::separator() + "appdata");
    if ( file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out << *m_hostList;
    }

}

void AppInit::loadHosts()
{
    QFile file(appDataDir() + QDir::separator() + "appdata");
    if ( file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        in >> *m_hostList;
    }
}

