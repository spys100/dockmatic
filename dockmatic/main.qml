/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/


import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Material 0.1
import com.ximidi.ContainerModel 1.0


ApplicationWindow {
    id: appwin
    visible: true
    width: 640
    height: 480
    title: "Dockmatic"

    theme {
        primaryColor: Palette.colors["lightBlue"]["500"]
        primaryDarkColor: Palette.colors["lightBlue"]["700"]
        accentColor: Palette.colors["red"]["A200"]
        tabHighlightColor: "white"
    }

    property var sectionTitles: [ "Hosts", "Containers" ]
    property string selectedComponent: sectionTitles[0]
    property int timeout: 10

    initialPage: TabbedPage {
        id: page

        title: appwin.title

        //actionBar.maxActionCount: navDrawer.enabled ? 3 : 4

        actions: [
            Action {
                iconName: "awesome/plus_square_o"
                name: qsTr("Add Host")
                onTriggered: {
                    dockerHostList.createItem();
                }
            },

            Action {
                iconName: "image/color_lens"
                name: "Colors"
                onTriggered: colorPicker.show()
            }
        ]

        //backAction: navDrawer.action
        Tab {
            title: "Hosts"
            sourceComponent: DockerHostTab {
                id: hostTab
            }
        }

        Repeater {
            model: dockerHostList
            delegate: Tab {
                title: model.alias ? model.alias : model.hostName
                property ContainerModel containerList: model.containerModel
                sourceComponent: DockerContainerTab{
                    onStart: {
                        var dh = model.dockerHost;
                        console.log("start container",id);
                        dh.startContainer(id);
                    }
                    onStop: {
                        var dh = model.dockerHost;
                        console.log("stop container",id);
                        dh.stopContainer(id, timeout);
                    }
                    onRestart: {
                        var dh = model.dockerHost;
                        console.log("restart container",id);
                        dh.restartContainer(id, timeout);
                    }
                    onPause: {
                        var dh = model.dockerHost;
                        console.log("pause container ",id);
                        dh.pauseContainer(id);
                    }
                    onUnpause: {
                        var dh = model.dockerHost;
                        console.log("unpause container ",id);
                        dh.unpauseContainer(id);
                    }
                    onOpenTerminal: {
                        var dh = model.dockerHost;
                        console.log("open terminal container ",id);
                        dh.containerAttach(id, name);
                    }

                    Connections {
                        target: model.dockerHost
                        onReceivedJsonReply: {
                            if ( cmd == 6 ){
                                console.log(json);
                                var event = JSON.parse(json);
                                var idx = containerList.indexById(event.id);
                                //containerList[1].status;
                                globalSnackBar.open(event.from + " " + event.status);
                            }
                        }
                    }
                }
            }
        }
    }

    Snackbar {
        id: globalSnackBar
    }


    menuBar: MenuBar {
        Menu {
            title: qsTr("Connection")
            MenuItem {
                text: qsTr("&Connect")
                onTriggered: { dockerHost1.connectTo() }
                //enabled: dockerHost1.host != "" && dockerHost1.port != 0
            }
            MenuItem {
                text: qsTr("Load &Key")
                onTriggered: console.log("Key action triggered");
            }
            MenuItem {
                text: qsTr("Load &Certificate")
                onTriggered: console.log("Certificate action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

}
