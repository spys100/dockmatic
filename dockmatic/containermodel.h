/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#ifndef CONTAINERMODEL_H
#define CONTAINERMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "container.h"

class ContainerModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum SettingsRoles {
        IDRole = Qt::UserRole + 1,
        NamesRole,
        ImageRole,
        CommandRole,
        CreatedRole,
        PortsRole,
        LabelsRole,
        StatusRole
    };
    explicit ContainerModel(QObject *parent = 0);
    ContainerModel(const ContainerModel& other);
    ContainerModel& operator=(const ContainerModel& other);

    QHash<int, QByteArray> roleNames() const;

    virtual int rowCount(const QModelIndex &parent=QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const {Q_UNUSED(index);return Qt::ItemIsEditable;}

    virtual bool insertRows(int row, int count, const QModelIndex &parent);
    virtual bool removeRows(int row, int count, const QModelIndex &parent);

    Q_INVOKABLE QModelIndex indexById(const QString& id);

private:
    QList<Container> m_containerList;

signals:

public slots:
};

Q_DECLARE_METATYPE(ContainerModel)

#endif // CONTAINERMODEL_H
