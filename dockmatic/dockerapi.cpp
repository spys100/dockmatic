/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QAuthenticator>
#include <QApplication>
//#include <QSslKey>
//#include <QSslCertificate>
#include <QDebug>
#include <QLoggingCategory>
#include <QWebSocket>


#include "dockerapi.h"
#include "dockerhost.h"

Q_LOGGING_CATEGORY(dockerAPI,"docker.api")

QStringList defaultEventList({"pause", "restart", "start", "stop", "unpause"});

DockerAPI::DockerAPI(QObject *parent)
    :QObject(parent)
    //,m_host(host)
{

}

void DockerAPI::onReceivedJsonData(DC::DockerCommandId cmd, const QJsonDocument doc, const QString err)
{
    if (!doc.isEmpty()){
        emit receivedJsonReply(static_cast<int>(cmd), doc.toJson());
        qCDebug(dockerAPI) << doc;
    }
}

void DockerAPI::createWebsocket(const QUrl& url, const QString& name)
{
    QWebSocket* ws=new QWebSocket("http://localhost", QWebSocketProtocol::VersionLatest, this);
    ws->setSslConfiguration(m_sslCfg);
    connect(ws, &QWebSocket::sslErrors, this,
            [ws] (const QList<QSslError>& errors) {
            QSslError error;
            foreach (error, errors) {
                qCDebug(dockerAPI) << error;
                if (error.error() == QSslError::HostNameMismatch ){
                    qCDebug(dockerAPI) << error.certificate().subjectInfo(QSslCertificate::CommonName)
                             << "," << error.certificate().subjectAlternativeNames().values();
                    ws->ignoreSslErrors();
                }
            }
    });
    // this static cast is needed to select the right member function, because there is another
    // member error() with a different signature
    connect(ws, static_cast<void (QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error), this,
            [=] (QAbstractSocket::SocketError) {
                qCritical() << ws->errorString();
                emit error(ws->errorString());
                ws->deleteLater();
    });
/* debug stuff
    connect(ws, &QWebSocket::stateChanged, this,  [ws] (QAbstractSocket::SocketState state) {
            qCDebug(dockerAPI) << "ws state = " << state;
    });
*/
    connect(ws, &QWebSocket::connected, this,  [=] () {
            qCDebug(dockerAPI) << "ws connected";
            attachConsole(ws, name);
    });
    connect(ws, &QWebSocket::disconnected, this,  [ws] () {
            qCDebug(dockerAPI) << "ws disconnected";
            ws->deleteLater();
    });

    ws->open(url);
}

/**
 * GET /info

    Display system-wide information

    Example request:

        GET /info HTTP/1.1

    Example response:

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
             "Containers":11,
             "Images":16,
             "Driver":"btrfs",
             "DriverStatus": [[""]],
             "ExecutionDriver":"native-0.1",
             "KernelVersion":"3.12.0-1-amd64"
             "NCPU":1,
             "MemTotal":2099236864,
             "Name":"prod-server-42",
             "ID":"7TRN:IPZB:QYBB:VPBQ:UMPP:KARE:6ZNR:XE6T:7EWV:PKF4:ZOJD:TPYS",
             "Debug":false,
             "NFd": 11,
             "NGoroutines":21,
             "SystemTime": "2015-03-10T11:11:23.730591467-07:00"
             "NEventsListener":0,
             "InitPath":"/usr/bin/docker",
             "InitSha1":"",
             "IndexServerAddress":["https://index.docker.io/v1/"],
             "MemoryLimit":true,
             "SwapLimit":false,
             "IPv4Forwarding":true,
             "Labels":["storage=ssd"],
             "DockerRootDir": "/var/lib/docker",
             "HttpProxy": "http://test:test@localhost:8080"
             "HttpsProxy": "https://test:test@localhost:8080"
             "NoProxy": "9.81.1.160"
             "OperatingSystem": "Boot2Docker",
        }

    Status Codes:

        200 – no error
        500 – server error
 */

void DockerAPI::info()
{
    sendRequest(DC::info, "/info");
}

void DockerAPI::version()
{
   sendRequest(DC::version, "/version");
}

void DockerAPI::events(const QStringList& eventFilter)
{
    QString filter = "[\"" + eventFilter.join("\",\"") + "\"]";
    t_paramList params;
    params.append(t_stringPair("filters","{\"event\":"+ filter +"}"));
    sendRequest(DC::events, "/events", params);
}

/*
 * Query Parameters:

        all – 1/True/true or 0/False/false, Show all containers. Only running containers are shown by default (i.e., this defaults to false)
        limit – Show limit last created containers, include non-running ones.
        since – Show only containers created since Id, include non-running ones.
        before – Show only containers created before Id, include non-running ones.
        size – 1/True/true or 0/False/false, Show the containers sizes
        filters - a json encoded value of the filters (a map[string][]string) to process on the containers list. Available filters:
        exited=<int> -- containers with exit code of <int>
        status=(restarting|running|paused|exited)

    Status Codes:

        200 – no error
        400 – bad parameter
        500 – server error
 */

void DockerAPI::listContainers()
{
    t_paramList params;
    params.append(t_stringPair("all","1"));
    sendRequest(DC::containerList, "/containers/json", params);
}

void DockerAPI::createContainter()
{

}

void DockerAPI::containerChanges()
{

}

/*
    POST /containers/(id)/start

    Start the container id

    Example request:

        POST /containers/(id)/start HTTP/1.1
        Content-Type: application/json

    Example response:

        HTTP/1.1 204 No Content

    Json Parameters:

    Status Codes:

        204 – no error
        304 – container already started
        404 – no such container
        500 – server error
 */
void DockerAPI::startContainer(const ID &id)
{
    sendRequest(DC::containerStart,"/containers/" + id + "/start");
}

/*
 *    POST /containers/(id)/stop

    Stop the container id

    Example request:

        POST /containers/e90e34656806/stop?t=5 HTTP/1.1

    Example response:

        HTTP/1.1 204 No Content

    Query Parameters:

        t – number of seconds to wait before killing the container

    Status Codes:

        204 – no error
        304 – container already stopped
        404 – no such container
        500 – server error
*/

void DockerAPI::stopContainer(const ID &id, int timeout)
{
    t_paramList params;
    params.append(t_stringPair("t", QString::number(timeout)));
    sendRequest(DC::containerStop, QString("/containers/%1/stop").arg(id), params);
}
/*
 * POST /containers/(id)/restart

    Restart the container id

    Example request:

        POST /containers/e90e34656806/restart?t=5 HTTP/1.1

    Example response:

        HTTP/1.1 204 No Content

    Query Parameters:

        t – number of seconds to wait before killing the container

    Status Codes:

        204 – no error
        404 – no such container
        500 – server error
*/
void DockerAPI::restartContainer(const ID &id, int timeout)
{
    t_paramList params;
    params.append(t_stringPair("t", QString::number(timeout)));
    sendRequest(DC::containerStop, QString("/containers/%1/restart").arg(id), params);
}

/*  POST /containers/(id)/pause

    Pause the container id

    Example request:

        POST /containers/e90e34656806/pause HTTP/1.1

    Example response:

        HTTP/1.1 204 No Content

    Status Codes:

        204 – no error
        404 – no such container
        500 – server error
*
*/
void DockerAPI::pauseContainer(const ID &id)
{
    sendRequest(DC::containerPause,QString("/containers/%1/pause").arg(id));
}

/*   POST /containers/(id)/unpause

    Unpause the container id

    Example request:

        POST /containers/e90e34656806/unpause HTTP/1.1

    Example response:

        HTTP/1.1 204 No Content

    Status Codes:

        204 – no error
        404 – no such container
        500 – server error
*/
void DockerAPI::unpauseContainer(const ID &id)
{
    sendRequest(DC::containerPause,QString("/containers/%1/unpause").arg(id));
}

/*
 *  GET /containers/(id)/attach/ws

    Attach to the container id via websocket

    Implements websocket protocol handshake according to RFC 6455

    Example request

        GET /containers/e90e34656806/attach/ws?logs=0&stream=1&stdin=1&stdout=1&stderr=1 HTTP/1.1

    Example response

        {{ STREAM }}

    Query Parameters:

        logs – 1/True/true or 0/False/false, return logs. Default false
        stream – 1/True/true or 0/False/false, return stream. Default false
        stdin – 1/True/true or 0/False/false, if stream=true, attach to stdin. Default false
        stdout – 1/True/true or 0/False/false, if logs=true, return stdout log, if stream=true, attach to stdout. Default false
        stderr – 1/True/true or 0/False/false, if logs=true, return stderr log, if stream=true, attach to stderr. Default false

    Status Codes:

        200 – no error
        400 – bad parameter
        404 – no such container
        500 – server error

 */

void DockerAPI::containerAttach(ID id, const QString& displayName)
{
    t_paramList params;
    params.append(t_stringPair("stream","true"));
    params.append(t_stringPair("stdin","true"));
    params.append(t_stringPair("stdout","true"));
    params.append(t_stringPair("stderr","true"));

    QUrl url = m_baseUrl;

    QUrlQuery query;
    query.setQueryItems(params);
    url.setPath(QString("/containers/%1/attach/ws").arg(id));
    url.setQuery(query);
    url.setScheme("wss");

    createWebsocket(url, displayName);
    //sendRequest(DC::containerAttach, QString("/containers/%1/attach/ws").arg(id), params);
}

void DockerAPI::containers_copy(ID id)
{

}
