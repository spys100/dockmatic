#include "customnetworkaccessmanager.h"
#include "httpnetworkreplylocalsocket.h"

CustomNetworkAccessManager::CustomNetworkAccessManager()
{

}

QNetworkReply *CustomNetworkAccessManager::createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
{
    if (request.url().userInfo().startsWith('/')){
        // local socket connection
       return new HttpNetworkReplyLocalSocket(this, op, request, outgoingData);
    }
    return QNetworkAccessManager::createRequest(op, request, outgoingData);
}
