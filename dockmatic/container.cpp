/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include "container.h"
#include <QDebug>

Container::Container()
{

}

Container::Container(const QString &id, const QStringList &names, const QString &image,
                     const QString &command, const QDateTime &created, const QList<int> &ports,
                     const QStringList &labels, const QString &status)
    : m_ID(id)
    , m_names(names)
    , m_image(image)
    , m_command(command)
    , m_created(created)
    , m_ports(ports)
    , m_labels(labels)
    , m_status(status)
{

}

QString Container::id() const
{
    return m_ID;
}

void Container::setId(const QString &id)
{
    m_ID = id;
}

QStringList Container::names() const
{
    return m_names;
}

void Container::setNames(const QStringList &names)
{
    m_names = names;
    qDebug() << this << m_names;
}

QString Container::image() const
{
    return m_image;
}

void Container::setImage(const QString &image)
{
    m_image = image;
}

QString Container::command() const
{
    return m_command;
}

void Container::setCommand(const QString &command)
{
    m_command = command;
}

QDateTime Container::created() const
{
    return m_created;
}

void Container::setCreated(const QDateTime &created)
{
    m_created = created;
}

QList<int> Container::ports() const
{
    return m_ports;
}

void Container::setPorst(const QList<int> &ports)
{
    m_ports = ports;
}

QStringList Container::labels() const
{
    return m_labels;
}

void Container::setLabels(const QStringList &labels)
{
    m_labels = labels;
}

QString Container::status() const
{
    return m_status;
}

void Container::setStatus(const QString &status)
{
    m_status = status;
}

