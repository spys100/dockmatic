/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

import QtQuick 2.3
import Material 0.1
import Material.ListItems 0.1 as ListItem
import com.ximidi.ContainerModel 1.0

Item {
    id: containerTab
    signal start(string id);
    signal stop(string id);
    signal restart(string id);
    signal pause(string id);
    signal unpause(string id);
    signal openTerminal(string id, string name);

    Flickable {
        id: flickable
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        clip: true
        contentHeight: Math.max(appwin.implicitHeight + 40, height)
        Column {
            anchors {
                left: parent.left
                right: parent.right
                margins: Units.dp(16)
            }
            Repeater {
                id: repeater
                anchors {
                    left: parent.left
                    right: parent.right
                }
                /// the model is defined externally in containermodel.cpp
                model: containerList
                Column{
                    width: parent.width
                    DockerContainer {
                        //id: header
                        name: model.names[0]
                        labels: model.labels
                        status:  model.status
                        onStart: {
                            containerTab.start(model.id);
                        }
                        onStop: {
                            containerTab.stop(model.id);
                        }
                        onRestart: {
                            containerTab.restart(model.id);
                        }
                        onPause: {
                            containerTab.pause(model.id);
                        }
                        onUnpause: {
                            containerTab.unpause(model.id);

                        }
                        onOpenTerminal: {
                            containerTab.openTerminal(model.id, name);
                        }
                    }
                    ThinDivider {
                        anchors {
                            left: parent.left
                            right: parent.right
                        //    top: parent.top
                        }
                    }
                }
            }
        }
    }
    Scrollbar {
        flickableItem: flickable
    }
}

