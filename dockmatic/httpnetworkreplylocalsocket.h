#ifndef HTTPNETWORKREPLYLOCALSOCKET_H
#define HTTPNETWORKREPLYLOCALSOCKET_H

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QLocalSocket>
#include <QList>
#include <QVariant>

class HttpNetworkReplyLocalSocket: public QNetworkReply
{
    Q_OBJECT
private:
    //static QHash<QString, QLocalSocket*> m_socks;
    static QList<QLocalSocket*> m_socks;
    QLocalSocket *m_sock;
    QByteArray m_content;
   // qint64 m_contentBytesLeft;
    enum ReadMode { HEADER, CONTENT };
    ReadMode m_readMode;
    qint64 readDataInternal();
    qint64 readChunked();

public:
    HttpNetworkReplyLocalSocket(const QNetworkAccessManager *manager,
                                QNetworkAccessManager::Operation op,
                                const QNetworkRequest &request,
                                QIODevice *outgoingData);

    virtual ~HttpNetworkReplyLocalSocket();
protected:
    virtual qint64 readData(char *data, qint64 maxlen) Q_DECL_OVERRIDE;
    virtual void abort() Q_DECL_OVERRIDE;

    void parseHeader();

protected slots:

    void onDataAvailable();
};

#endif // HTTPNETWORKREPLYLOCALSOCKET_H
