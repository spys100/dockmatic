/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include "dockerhost.h"
#include <QDataStream>
#include <QSslKey>

#include <QNetworkReply>
#include <QUrlQuery>
//#include <QWebSocket>
#include <QAuthenticator>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QLoggingCategory>

#include "appinit.h"
#include "dockerapi.h"
#include "containermodel.h"
#include "terminal.h"
#include "customnetworkaccessmanager.h"

#define QNETWORKREQ_COMMAND QNetworkRequest::Attribute(int(QNetworkRequest::User))
Q_DECLARE_LOGGING_CATEGORY( dockerAPI )

QDataStream &operator<<(QDataStream &stream, const DockerHost &dh)
{
    stream << dh.hostName() << dh.port();
    stream << dh.cert() << dh.key() << dh.ca();
    stream << dh.alias();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DockerHost &dh)
{
    QString val;
    QByteArray baVal;
    int iVal;

    stream >> val; dh.setHostName(val);
    stream >> iVal; dh.setPort(iVal);
    stream >> baVal; dh.setCert(baVal);
    stream >> baVal; dh.setKey(baVal);
    stream >> baVal; dh.setCa(baVal);
    stream >> val; dh.setAlias(val);
    return stream;
}

QDataStream &operator<< (QDataStream& stream, const DockerHostPtr& dh)
{
    stream << *dh;
    return stream;
}

QDataStream &operator>> (QDataStream& stream, DockerHostPtr& dh)
{
    dh = DockerHostPtr::create();
    stream >> *dh;
    return stream;
}

CustomNetworkAccessManager* DockerHost::m_networkmanager = NULL;

DockerHost::DockerHost()
    :m_containerModel(new ContainerModel(this))
{
    // default values
    //m_baseUrl.setScheme("https");
    //setHostName("192.168.59.103");
    m_baseUrl.setScheme("http");
    setHostName("/var/run/docker.sock");
    //setPort(2377);

    if (! m_networkmanager) {
        m_networkmanager = new CustomNetworkAccessManager();
    }
}


QByteArray DockerHost::key() const {
    return m_sslCfg.privateKey().toPem();
}

void DockerHost::setKey(const QByteArray &val)
{
    m_sslCfg.setPrivateKey(QSslKey(val,QSsl::Rsa));
}

QByteArray DockerHost::cert() const
{
            return m_sslCfg.localCertificate().toPem();
}

void DockerHost::setCert(const QByteArray &val)
{
    m_sslCfg.setLocalCertificate(QSslCertificate(val));
}

QByteArray DockerHost::ca() const
{
    QByteArray rv;
    if ( ! m_sslCfg.caCertificates().isEmpty() )
        rv = m_sslCfg.caCertificates().first().toPem();
    return rv;
}

void DockerHost::setCa(const QByteArray &val)
{
    QList<QSslCertificate> caList;
    caList.append(QSslCertificate(val));
    m_sslCfg.setCaCertificates(caList);
}

void DockerHost::connectTo()
{

    qCDebug(dockerAPI) << "networkmanager supported schemes: " << m_networkmanager->supportedSchemes();
    connect(m_networkmanager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));
    connect(m_networkmanager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)), SLOT(authenticate(QNetworkReply*,QAuthenticator*)));
    connect(m_networkmanager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), SLOT(onSslErrors(QNetworkReply*,QList<QSslError>)));

    m_networkmanager->connectToHostEncrypted(m_baseUrl.host(), m_baseUrl.port(), m_sslCfg);
}

void DockerHost::onSslErrors(QNetworkReply *reply, QList<QSslError> errors)
{
    QSslError error;
    foreach (error, errors) {
        qCDebug(dockerAPI) << error.certificate().subjectInfo(QSslCertificate::CommonName)
                 << "," << error.certificate().subjectAlternativeNames().values()
                 << ":" << error;
        if (error.error() == QSslError::HostNameMismatch )
            reply->ignoreSslErrors();
    }
}

void DockerHost::attachConsole(QWebSocket *ws, const QString& title)
{
    TerminalProxy* tproxy = new TerminalProxy(ws, title);
    //connect(tproxy, );
}

void DockerHost::replyFinished(QNetworkReply* reply)
{
    QJsonParseError err;
    QJsonDocument doc;
    QByteArray ba;

    DC::DockerCommandId cid;

    cid = reply->request().attribute(QNETWORKREQ_COMMAND).value<DC::DockerCommandId>();

    qDebug("replyFinished cid: %i", cid);

    //check for errors
    if (reply->error() != QNetworkReply::NoError) {
        qCritical() <<  reply->errorString();
        emit error(reply->errorString());
    }
    //check response code
    int status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    qCDebug(dockerAPI, "Reveived Status Response %d", status);
    if ( status != 200 ){

    }

    ba = reply->readAll();
    doc = QJsonDocument::fromJson(ba,&err);

    switch (cid){
    case DC::containerList: {
        //populate container model
        foreach(QJsonValue val, doc.array()){
            Q_ASSERT(val.isObject());
            addContainer(val.toObject());
        }
    };break;
    case DC::events:{
        //update container status
        QModelIndex i = containerModel()->indexById(doc.object()["id"].toString());
        containerModel()->setData(i, doc.object()["status"].toString(), ContainerModel::StatusRole);
    };break;
    }

    onReceivedJsonData(cid, doc, err.errorString());
    if (cid != DC::events){
        reply->deleteLater();
    }
}

void DockerHost::onDockerEvent()
{
    replyFinished(m_eventReply);
}

void DockerHost::addContainer(const QJsonObject &container)
{
    m_containerModel->insertRow(0);
    QModelIndex i = m_containerModel->index(0);
    QVariant val;
    QHash<int, QByteArray> roles = m_containerModel->roleNames();

    QJsonObject::const_iterator it;
    for ( it = container.constBegin(); it != container.constEnd(); it++) {
        int rnum =  roles.key(it.key().toLower().toUtf8());
        QStringList stringList;
        if ( it.value().isArray() ){
            foreach (QJsonValue jv, it.value().toArray()){
                stringList.append(jv.toString());
            }
            val = stringList;
        }else{
            val = it.value();
        }
        m_containerModel->setData(i, val, rnum);
    }
    //int c = m_containerModel->rowCount();
}

void DockerHost::sendRequest(DC::DockerCommandId cid, const QString& path, const t_paramList &params)
{
    QNetworkRequest req;
    QNetworkReply *reply;
    QUrl url=m_baseUrl;
    QUrlQuery query;

    query.setQueryItems(params);

    url.setPath(path);
    url.setQuery(query);

    qDebug("sendRequest cid: %i, uri: %s, ", cid, url.toDisplayString().toUtf8().data());

    {
        req.setSslConfiguration(m_sslCfg);
        req.setAttribute(QNETWORKREQ_COMMAND, QVariant::fromValue(cid));
        req.setAttribute(QNetworkRequest::CacheSaveControlAttribute, false);
        req.setUrl(url);
        if (cid > DC::POSTREQUEST) {
            //send as POST
            QByteArray data;
            reply = m_networkmanager->post(req, data);
        }else{
            reply = m_networkmanager->get(req);
        }

        if (cid == DC::events){
            m_eventReply = reply;
            connect(reply, SIGNAL(readyRead()), SLOT(onDockerEvent()));
        }
    }
}

void DockerHost::authenticate(QNetworkReply *repl, QAuthenticator *auth)
{
    Q_UNUSED(repl);
    qCDebug(dockerAPI) << auth->realm();
}
