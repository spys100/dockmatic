/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include "dockerhostmodel.h"
#include "dockerhost.h"
#include <QDataStream>

QDataStream &operator<<(QDataStream &stream, const DockerHostModel &model)
{
    stream << model.m_hostList;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DockerHostModel &model)
{ 
    stream >> model.m_hostList;
    return stream;
}

DockerHostModel::DockerHostModel(QObject *parent) : QAbstractListModel(parent)
{

}

QHash<int, QByteArray> DockerHostModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[HostNameRole] = "hostName";
    roles[PortRole] = "port";
    roles[KeyRole] = "key";
    roles[CertRole] = "cert";
    roles[CaRole] = "ca";
    roles[AliasRole] = "alias";
    roles[ContainerModelRole] = "containerModel";
    roles[DockerHostRole] = "dockerHost";
    return roles;
}

int DockerHostModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_hostList.count();
}

QVariant DockerHostModel::data(const QModelIndex &index, int role) const
{
    QVariant val;
    if (index.isValid()){
        const DockerHostPtr dh = m_hostList.at(index.row());

        switch (role) {
        case HostNameRole:
            val.setValue(dh->hostName());
            break;
        case PortRole:
            val.setValue(dh->port());
            break;
        case KeyRole:
            val.setValue(dh->key());
            break;
        case CertRole:
            val.setValue(dh->cert());
            break;
        case CaRole:
            val.setValue(dh->ca());
            break;
        case AliasRole:
            val.setValue(dh->alias());
            break;
        case ContainerModelRole:{
            val.setValue(dh->containerModel());
        };break;
        case DockerHostRole:
            val.setValue(dh.data());
            break;
        default:
            break;
        }
    }
    return val;
}

bool DockerHostModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    QVector<int> roles;
    roles.append(role);
    DockerHostPtr dh = m_hostList[index.row()];
    switch (role) {
    case HostNameRole:
        dh->setHostName(value.toString());
        break;
    case PortRole:
        dh->setPort(value.toInt());
        break;
    case KeyRole:
        dh->setKey(value.toByteArray());
        break;
    case CertRole:
        dh->setCert(value.toByteArray());
        break;
    case CaRole:
        dh->setCa(value.toByteArray());
        break;
    case AliasRole:
        dh->setAlias(value.toString());
        break;
    }

    emit dataChanged(index, index, roles);
    return true;
}

bool DockerHostModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent,row, row + count - 1);
    for (; count > 0; count--)
        m_hostList.insert(row, DockerHostPtr(new DockerHost()));
    endInsertRows();
    return true;
}

bool DockerHostModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent,row, row + count - 1);
    for (; count > 0; count--)
        m_hostList.removeAt(row + count - 1);
    endRemoveRows();
    return true;

}

QList<DockerHostPtr> DockerHostModel::model() const
{
    return m_hostList;
}

void DockerHostModel::createItem()
{
    int row=rowCount();
    insertRow(row);
    //setData(createIndex(row,0), "", HostNameRole);
}

void DockerHostModel::removeItem(int i)
{
    removeRow(i);
}

