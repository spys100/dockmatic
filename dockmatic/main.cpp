/*
    This file is part of Dockmatic.
    Copyright (C) 2015 by Carsten Kroll <carsten (at) ximidi.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.

*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "dockerapi.h"
#include "terminal.h"
#include "dockerhostmodel.h"
#include "containermodel.h"
#include "dockerhost.h"
#include "filereader.h"
#include "appinit.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(colorschemes);

    qRegisterMetaType<ContainerModel>();
    qRegisterMetaType<ID>("ID");

    AppInit app(argc, argv);

    //qmlRegisterSingletonType<DockerAPI>("com.docker.DockerAPI", 1, 0, "DockerAPI", AppInit::dockerAPI);
    //qmlRegisterType<TerminalProxy>("org.qt.qtermwidget", 1, 0, "QTermWidget");
    qmlRegisterType<DockerHostModel>("com.ximidi.DockerHostModel", 1, 0 ,"DockerHostModel");
    qmlRegisterType<DockerHost>("com.ximidi.DockerHost", 1, 0 ,"DockerHost");
    qmlRegisterType<ContainerModel>("com.ximidi.ContainerModel", 1, 0 ,"ContainerModel");
    qmlRegisterType<FileReader>("com.ximidi.FileReader", 1, 0 ,"FileReader");



    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/"); 

    DockerHostModel *modelData = app.hostList();

    engine.rootContext()->setContextProperty("dockerHostList", modelData);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));



    //createConsole();

    return app.exec();
}
