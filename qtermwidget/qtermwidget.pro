#-------------------------------------------------
#
# Project created by QtCreator 2015-10-04T22:45:30
#
#-------------------------------------------------

QT       += widgets xml

QT       -= gui

TARGET = qtermwidget
TEMPLATE = lib
CONFIG += staticlib c++11
DEFINES += HAVE_UTMPX _UTMPX_COMPAT HAVE_POSIX_OPENPT HAVE_SYS_TIME_H \
    BUNDLE_KEYBOARDLAYOUTS \
    BUNDLE_COLORSCHEMES

macx-clang:QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-inconsistent-missing-override

unix {
    target.path = /usr/lib
    INSTALLS += target
}

HEADERS += \
    lib/BlockArray.h \
    lib/Character.h \
    lib/CharacterColor.h \
    lib/ColorScheme.h \
    lib/ColorTables.h \
    lib/DefaultTranslatorText.h \
    lib/Emulation.h \
    lib/ExtendedDefaultTranslator.h \
    lib/Filter.h \
    lib/History.h \
    lib/HistorySearch.h \
    lib/KeyboardTranslator.h \
    lib/konsole_wcwidth.h \
    lib/kprocess.h \
    lib/kpty.h \
    lib/kpty_p.h \
    lib/kptydevice.h \
    lib/kptyprocess.h \
    lib/LineFont.h \
    lib/Pty.h \
    lib/qtermwidget.h \
    lib/Screen.h \
    lib/ScreenWindow.h \
    lib/SearchBar.h \
    lib/Session.h \
    lib/ShellCommand.h \
    lib/TerminalCharacterDecoder.h \
    lib/TerminalDisplay.h \
    lib/tools.h \
    lib/Vt102Emulation.h

SOURCES += \
    lib/BlockArray.cpp \
    lib/ColorScheme.cpp \
    lib/Emulation.cpp \
    lib/Filter.cpp \
    lib/History.cpp \
    lib/HistorySearch.cpp \
    lib/KeyboardTranslator.cpp \
    lib/konsole_wcwidth.cpp \
    lib/kprocess.cpp \
    lib/kpty.cpp \
    lib/kptydevice.cpp \
    lib/kptyprocess.cpp \
    lib/Pty.cpp \
    lib/qtermwidget.cpp \
    lib/Screen.cpp \
    lib/ScreenWindow.cpp \
    lib/SearchBar.cpp \
    lib/Session.cpp \
    lib/ShellCommand.cpp \
    lib/TerminalCharacterDecoder.cpp \
    lib/TerminalDisplay.cpp \
    lib/tools.cpp \
    lib/Vt102Emulation.cpp

FORMS += \
    lib/SearchBar.ui

RESOURCES += \
    lib/color-schemes/colorschemes.qrc
